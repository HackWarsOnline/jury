TARGET = jury
TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
MOC_DIR = tmp
OBJECTS_DIR = tmp

QT += core sql
QT -= gui

INCLUDEPATH += $$PWD/src/models \
            $$PWD/src \
            $$PWD/src/utils \
            $$PWD/src/interfaces \
            $$PWD/src/storage \
            $$PWD/src/3rd_party/INIReader \
            $$PWD/src/scoreboard \

# #######################
# INIReader

SOURCES += \
src/3rd_party/INIReader/ini.c \
src/3rd_party/INIReader/INIReader.cpp \

HEADERS += \
src/3rd_party/INIReader/ini.h \
src/3rd_party/INIReader/INIReader.h \

# #######################
# Project 

SOURCES += \
src/main.cpp \
src/service_checker_thread.cpp \
src/config.cpp \
src/models/model_team.cpp \
src/models/model_service.cpp \
src/utils/utils_logger.cpp \
src/utils/utils_light_http_server.cpp \
src/storage/mysql_storage.cpp \
src/scoreboard/http_handler.cpp \

HEADERS += \
src/service_checker_thread.h \
src/config.h \
src/models/model_team.h \
src/models/model_service.h \
src/utils/utils_logger.h \
src/utils/utils_light_http_server.h \
src/utils/colormod.h \
src/interfaces/istorage.h \
src/storage/mysql_storage.h \
src/scoreboard/http_handler.h \


