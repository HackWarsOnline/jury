#include "config.h"
#include "utils_logger.h"
#include <QSettings>
#include <QTextStream>
#include <QFile>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <mysql_storage.h>

JuryConfiguration::JuryConfiguration(){
    TAG = "JuryConfiguration";
    m_nFlagTimeliveInMin = 10;
    m_nDatabasePort = 3306;
    m_nScoreboardPort = 4444;
    m_pStorage = NULL;
}

// ---------------------------------------------------------------------

bool JuryConfiguration::readFromFile(QString sConfigFile){
    bool bResult = true;
    Log::info(TAG, "Loading configuration... ");
    Log::info(TAG, "Config File: /usr/share/relertjury/conf.d/conf.ini");

    QSettings settings(sConfigFile, QSettings::IniFormat);

    if(!settings.contains("server/use_storage")){
        Log::err(TAG, "server/use_storage - not found");
		return false;
	}

    if(!settings.contains("server/scoreboard_port")){
        Log::err(TAG, "server/scoreboard_port - not found");
		return false;
	}

    if(!settings.contains("server/scoreboard_html_folder")){
        Log::err(TAG, "server/scoreboard_html_folder - not found");
		return false;
	}

    m_nScoreboardPort = settings.value("server/scoreboard_port").toInt();
    m_sScoreboardHtmlFolder = settings.value("server/scoreboard_html_folder").toString().toStdString();

    QString sStorage = settings.value("server/use_storage").toString();
    if(sStorage == "mysql"){
        // TODO
        /*MySqlStorage *pMySqlStorage = new MySqlStorage();
        // 
        m_pStorage = (IStorage *)pMySqlStorage;
        if(!m_pStorage->readFromFile(sConfigFile.toStdString())){
            Log::err(TAG, "Could not init configuration storage");
            return false;
        }*/
    }

    if(!settings.contains("mysql_storage/dbhost")){
        Log::err(TAG, "database/dbhost - not found");
		return false;
	}
    
    if(!settings.contains("mysql_storage/dbport")){
		Log::err(TAG, "database/dbport - not found");
		return false;
	}

    if(!settings.contains("mysql_storage/dbname")){
		Log::err(TAG, "database/dbname - not found");
		return false;
	}

	if(!settings.contains("mysql_storage/dbuser")){
		Log::err(TAG, "database/dbuser - not found");
		return false;
	}

    if(!settings.contains("mysql_storage/dbpass")){
		Log::err(TAG, "database/dbuser - not found");
		return false;
	}

    m_sDatabaseHost = settings.value("mysql_storage/dbhost").toString();
    m_nDatabasePort = settings.value("mysql_storage/dbport").toInt();
	m_sDatabaseName = settings.value("mysql_storage/dbname").toString();
	m_sDatabaseUser = settings.value("mysql_storage/dbuser").toString();
	m_sDatabasePass = settings.value("mysql_storage/dbpass").toString();

    Log::info(TAG, "DatabaseName: " + m_sDatabaseName);
    Log::info(TAG, "DatabaseUser: " + m_sDatabaseUser);

    m_nFlagTimeliveInMin = settings.value("game/flag_timelive_in_min").toInt();
    m_sGameName = settings.value("game/name").toString();

    for(int nNum = 1; nNum < 25; nNum++){ // max 25 services
        QString serviceN = "service" + QString::number(nNum);
        if(settings.contains(serviceN + "/name")){
            ModelService service;
            service.setId(serviceN);
            service.setNum(nNum);
            service.setName(settings.value(serviceN + "/name").toString());
            service.setScriptPath(settings.value(serviceN + "/script_path").toString());
            QString sEnabled = settings.value(serviceN + "/enabled").toString().toUpper();
            service.setEnabled(sEnabled == "YES" || sEnabled == "");
            service.setScriptWaitInSec(settings.value(serviceN + "/script_wait_in_sec").toInt());
            service.setTimeSleepBetweenRunScriptsInSec(settings.value(serviceN + "/time_sleep_between_run_scripts_in_sec").toInt());
            if(service.isEnabled()){
                m_vServices.push_back(service);
                Log::info(TAG, "Registred " + service.id() + " : " + service.name());
            }else{
                Log::warn(TAG, "Disbaled " + service.id() + " : " + service.name());
            }
        }
    }

    if(m_vServices.size() == 0){
        bResult = false;
        Log::err(TAG, "Services does not defined");
        return false;
    }

    for(int nNum = 1; nNum < 200; nNum++){ // max 200 teams
        QString teamN = "team" + QString::number(nNum);
        if(settings.contains(teamN + "/name")){
            ModelTeam team;
            team.setId(teamN);
            team.setNum(nNum);
            team.setName(settings.value(teamN + "/name").toString());
            team.setLogo(settings.value(teamN + "/logo").toString());
            team.setIpAddress(settings.value(teamN + "/ip_address").toString());
            QString sActive = settings.value(teamN + "/active").toString().toUpper();
            team.setActive(sActive == "YES" || sActive == "");
            if(team.isActive()){
                m_vTeams.push_back(team);
                Log::info(TAG, "Registred " + team.id()
                    + " : " + team.name() + " (ip address: " + team.ipAddress() + ")");
            }else{
                Log::warn(TAG, "Inactive " + team.id()
                    + " : " + team.name() + " (ip address: " + team.ipAddress() + ")");
            }
        }
    }

    if(m_vTeams.size() == 0){
        Log::err(TAG, "Teams does not defined");
        return false;
    }

    return bResult;
}

// ---------------------------------------------------------------------

bool JuryConfiguration::exportToConfigPHP(){
    QFile file("/usr/share/relertjury/html/config.php");
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        return false;
    }
    QTextStream stream( &file );
    stream << "<?php " << endl;
    stream << "/* Automaticly generated by jury on start */ " << endl;
    stream << "$database = array(); " << endl;
    stream << "$database['name'] = '" << databaseName() << "'; " << endl;
    stream << "$database['user'] = '" << databaseUser() << "'; " << endl;
    stream << "$database['pass'] = '" << databasePass() << "'; " << endl;

    stream << "$game = array("
        << "  'name' => '" << gameName().toHtmlEscaped() << "',"
        << "); " << endl;

    stream << "$services = array(); " << endl;
    for(int i = 0; i < m_vServices.size(); i++){
        stream << "$services[] = array(" << endl
            << "  'id' => '" << m_vServices[i].id().toHtmlEscaped() << "'," << endl
            << "  'name' => '" << m_vServices[i].name().toHtmlEscaped() << "'," << endl
        << "); " << endl;
    }

    stream << "$teams = array(); " << endl;
    for(int iteam = 0; iteam < m_vTeams.size(); iteam++){
        stream << "$teams['" << m_vTeams[iteam].id() << "'] = array(" << endl
            << "  'id' => '" << m_vTeams[iteam].id() << "'," << endl
            << "  'place' => 0," << endl
            << "  'ip_address' => '" << m_vTeams[iteam].ipAddress() << "', " << endl
            << "  'name' => '" << m_vTeams[iteam].name().toHtmlEscaped() << "'," << endl
            << "  'logo' => '" << m_vTeams[iteam].logo().toHtmlEscaped() << "'," << endl
            << "  'Defence' => 0," << endl
            << "  'Lost' => 0," << endl
            << "  'Attack' => 0," << endl
            << "  'Summary' => 0," << endl;
            for(int iservice = 0; iservice < m_vServices.size(); iservice++){
                stream << "  '" << m_vServices[iservice].id().toHtmlEscaped() << "' => array(" << endl
                    << "    'id' => '" << m_vServices[iservice].id().toHtmlEscaped() << "'," << endl
                    << "    'name' => '" << m_vServices[iservice].name().toHtmlEscaped() << "'," << endl
                    << "    'service_status' => ''," << endl
                << "  ), " << endl;
            }

        stream << "); " << endl;
    }

    file.close();
	return true;
}

// ---------------------------------------------------------------------

bool JuryConfiguration::initScoreboardTable(){
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
	db.setHostName(databaseHost());
    db.setPort(databasePort());
	db.setDatabaseName(databaseName());
	db.setUserName(databaseUser());
	db.setPassword(databasePass());

	if (!db.open()){
		Log::err(TAG, db.lastError().text());
		Log::err(TAG, "Failed to connect.");
		return false;
	}

    for(int iteam = 0; iteam < m_vTeams.size(); iteam++){
        QVector<QString> other;
        other.push_back("Defence");
        other.push_back("Lost");
        other.push_back("Attack");
        other.push_back("Summary");

        for(int iservice = 0; iservice < m_vServices.size(); iservice++){
            other.push_back(m_vServices[iservice].id());
        }
        for (int i = 0; i < other.size(); i++){
            QString sName = other[i];
            QSqlQuery query(db);
            query.prepare("SELECT * FROM scoreboard WHERE name = :name AND teamid = :teamid");
            query.bindValue(":name", sName);
            query.bindValue(":teamid", m_vTeams[iteam].num());
            if(!query.exec()){
                Log::err(TAG, "Query failed \n\n" + query.lastError().text());
                return false;
            }

            if(!query.next()) {
                QSqlQuery query_insert(db);
                query_insert.prepare("INSERT INTO scoreboard(name, teamid, score, service_status, date_change) VALUES(:name, :teamid, 0,'',NOW())");
                query_insert.bindValue(":name", sName);
                query_insert.bindValue(":teamid", m_vTeams[iteam].num());
                if(!query_insert.exec()){
                    Log::err(TAG, "Query failed \n\n" + query_insert.lastError().text());
                    return false;
                }
            }
        }
    }
    return true;
}

// ---------------------------------------------------------------------

int JuryConfiguration::flagTimeliveInMin(){
    return m_nFlagTimeliveInMin;
}

// ---------------------------------------------------------------------

QVector<ModelTeam> &JuryConfiguration::teams(){
    return m_vTeams;
}

// ---------------------------------------------------------------------

QVector<ModelService> &JuryConfiguration::services(){
    return m_vServices;
}

// ---------------------------------------------------------------------

int JuryConfiguration::scoreboardPort(){
    return m_nScoreboardPort;
}

// ---------------------------------------------------------------------

std::string JuryConfiguration::scoreboardHtmlFolder(){
    return m_sScoreboardHtmlFolder;
}

// ---------------------------------------------------------------------

QString JuryConfiguration::databaseHost(){
    return m_sDatabaseHost;
}

// ---------------------------------------------------------------------

int JuryConfiguration::databasePort(){
    return m_nDatabasePort;
}

// ---------------------------------------------------------------------

QString JuryConfiguration::databaseName(){
    return m_sDatabaseName;
}

// ---------------------------------------------------------------------

QString JuryConfiguration::databaseUser(){
    return m_sDatabaseUser;
}

// ---------------------------------------------------------------------

QString JuryConfiguration::databasePass(){
    return m_sDatabasePass;
}

// ---------------------------------------------------------------------

QString JuryConfiguration::gameName(){
    return m_sGameName;
}