#ifndef MODEL_TEAM_H
#define MODEL_TEAM_H

#include <QString>

class ModelTeam {
    public:
        ModelTeam();
        
        void setId(QString sId);
        QString id();

        void setNum(int nNum);
        int num();

        void setName(QString sName);
        QString name();

        void setIpAddress(QString sIpAddress);
        QString ipAddress();

        void setActive(bool bActive);
        bool isActive();

        void setLogo(QString sLogo);
        QString logo();

    private:
        int m_nNum;
        bool m_bActive;
        QString m_sTeamID;
        QString m_sName;
        QString m_sIpAddress;
        QString m_sLogo;
};

#endif // MODEL_TEAM_H
