#include "model_service.h"

ModelService::ModelService(){
    m_nScriptWaitInSec = 10;
    m_bEnabled = true;
    m_nTimeSleepBetweenRunScriptsInSec = 10;
}

void ModelService::setId(QString sServiceID){
    m_sID = sServiceID;
}

QString ModelService::id(){
    return m_sID;
}

void ModelService::setNum(int nNum){
    m_nNum = nNum;
}

int ModelService::num(){
    return m_nNum;
}

void ModelService::setName(QString sName){
    m_sName = sName;
}

QString ModelService::name(){
    return m_sName;
}

void ModelService::setScriptPath(QString sScriptPath){
    m_sScriptPath = sScriptPath;
}

QString ModelService::scriptPath(){
    return m_sScriptPath;
}

void ModelService::setEnabled(bool bEnabled){
    m_bEnabled = bEnabled;
}

bool ModelService::isEnabled(){
    return m_bEnabled;
}

void ModelService::setScriptWaitInSec(int nSec){
    m_nScriptWaitInSec = nSec;
    if(m_nScriptWaitInSec < 1){
        m_nScriptWaitInSec = 10;
    }
}

int ModelService::scriptWaitInSec(){
    return m_nScriptWaitInSec;
}

void ModelService::setTimeSleepBetweenRunScriptsInSec(int nSec){
    m_nTimeSleepBetweenRunScriptsInSec = nSec;
    if(m_nTimeSleepBetweenRunScriptsInSec < 1){
        m_nTimeSleepBetweenRunScriptsInSec = 10;
    }
}

int ModelService::timeSleepBetweenRunScriptsInSec(){
    return m_nTimeSleepBetweenRunScriptsInSec;
}