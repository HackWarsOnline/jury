#include "model_team.h"

ModelTeam::ModelTeam(){

}

void ModelTeam::setId(QString sTeamId){
    m_sTeamID = sTeamId;
}

QString ModelTeam::id(){
    return m_sTeamID;
}

void ModelTeam::setNum(int nNum){
    m_nNum = nNum;
}

int ModelTeam::num(){
    return m_nNum;
}

void ModelTeam::setName(QString sName){
    m_sName = sName;
}

QString ModelTeam::name(){
    return m_sName;
}

void ModelTeam::setIpAddress(QString sIpAddress){
    m_sIpAddress = sIpAddress;
}

QString ModelTeam::ipAddress(){
    return m_sIpAddress;
}

void ModelTeam::setActive(bool bActive){
    m_bActive = bActive;
}

bool ModelTeam::isActive(){
    return m_bActive;
}

void ModelTeam::setLogo(QString sLogo){
    m_sLogo = sLogo;
}

QString ModelTeam::logo(){
    return m_sLogo;
}