#ifndef MODEL_SERVICE_H
#define MODEL_SERVICE_H

#include <QString>

class ModelService {
    public:
        ModelService();
        
        void setId(QString sServiceId);
        QString id();

        void setNum(int nNum);
        int num();

        void setName(QString sName);
        QString name();

        void setScriptPath(QString sScriptPath);
        QString scriptPath();

        void setEnabled(bool bEnabled);
        bool isEnabled();

        void setScriptWaitInSec(int nSec);
        int scriptWaitInSec();

        void setTimeSleepBetweenRunScriptsInSec(int nSec);
        int timeSleepBetweenRunScriptsInSec();

    private:
        int m_nNum;
        bool m_bEnabled;
        int m_nScriptWaitInSec;
        int m_nTimeSleepBetweenRunScriptsInSec;
        QString m_sID;
        QString m_sName;
        QString m_sScriptPath;
};

#endif // MODEL_SERVICE_H
