#ifndef MYSQL_STORAGE_H
#define MYSQL_STORAGE_H

#include <istorage.h>

class MySqlStorage {
    public:
        MySqlStorage();
        virtual std::string type();
        virtual bool readFromFile(const std::string &sFilePath);
};

#endif // MYSQL_STORAGE_H