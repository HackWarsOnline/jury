#include "service_checker_thread.h"
#include <unistd.h>
#include <QtCore>
#include <QString>
#include <QFile>
#include <QVector>
#include <QThread>
#include <QTextStream>
#include <QMap>
#include <QList>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QProcess>
#include <QUuid>
#include <QRegExp>
#include <iostream>
#include "utils_logger.h"

ServiceCheckerThread::ServiceCheckerThread(const JuryConfiguration &config,
            const ModelTeam &team, const ModelService &service) {
    
    m_config = config;
    m_team = team;
    m_service = service;
    m_sConnectionName = m_team.id() + "_" + m_service.id();
    TAG = "Thread_" + m_sConnectionName;
}
 
void ServiceCheckerThread::run() {
    // TODO: BUG: so here can be problem with mysql connection after 7-8 hours (terminate connection on MySQL side)
    // SOLUTION migrate to PostgreSQL

    Log::info(TAG, "Starting thread " + m_sConnectionName);
    m_pDatabase = new QSqlDatabase(QSqlDatabase::addDatabase("QMYSQL", m_sConnectionName));
    m_pDatabase->setHostName(m_config.databaseHost());
    m_pDatabase->setPort(m_config.databasePort());
    m_pDatabase->setDatabaseName(m_config.databaseName());
    m_pDatabase->setUserName(m_config.databaseUser());
    m_pDatabase->setPassword(m_config.databasePass());
    if (!m_pDatabase->open()){
        Log::err(TAG, m_pDatabase->lastError().text());
        Log::err(TAG, "Failed to connect.");
        return;
    }

    while(1) {
    
        QString flag = QUuid::createUuid().toString();
        flag = flag.mid(1,flag.length()-2);
        
        if (m_team.ipAddress().isEmpty()) {
            Log::err(TAG, "FAIL: UserIP is empty!!!");
            return;
        }
        
        QFile file(m_service.scriptPath());
        if (!file.exists()) {
            Log::err(TAG, "FAIL: Script Path to checker not found '" + m_service.scriptPath() + "'");
            return;
        }

        QStringList args;
        args << m_team.ipAddress() << "put" << flag;
        
        Log::info(TAG, "Start script " + m_service.scriptPath() + "  " + m_team.ipAddress() + " flag " + flag);
        QProcess p;
        p.setProcessChannelMode(QProcess::MergedChannels);
        p.start(m_service.scriptPath(), args);
        if(p.waitForStarted(1000)){
            // Log::info(TAG, "Script started");
        }else{
            Log::err(TAG, "Script not started");
            Log::err(TAG, "p.error(): " + p.error());
        }
        
        if(p.waitForFinished(m_service.scriptWaitInSec()*1000)){
            // Log::info(TAG, "Script finished");
        }else{
            Log::info(TAG, "Not finished");
            Log::info(TAG, "p.error():" + p.error());
            p.kill();
            // update status of service - corrupted
        }
        
        QString p_stdout = p.readAllStandardOutput();
        QString p_stderr = p.readAllStandardError();
        
        if(p_stderr.length() > 0) {
            Log::err(TAG, "Error in call checker: '" + m_service.scriptPath() + "' p_stderr = " + p_stderr + "; p_stdout = " + p_stdout);
            // update status of service
        } else {
            QString p_output = p_stdout.toUpper();
            QRegExp rx_work("\\[SERVICE IS WORKED\\]");
            QRegExp rx_corrupt("\\[SERVICE IS CORRUPT\\]");

            if(rx_work.lastIndexIn(p_output) != -1) {
                updateFlags(flag, true);
                Log::ok(TAG, m_sConnectionName + " => service is worked");
                // writeToLog(db_cnf, srvs_cnf.strServiceName + ":" + QString::number(srvs_cnf.userID) + " => service is worked");
            } else if (rx_corrupt.lastIndexIn(p_output) != -1) {
                updateFlags(flag, false);
                Log::warn(TAG, m_sConnectionName + " => service is corrupt ");
                // insertToFlags(cnf, service_id, team_id, flag, "-1");
                // writeToLog(db_cnf, srvs_cnf.strServiceName + ":" + QString::number(srvs_cnf.userID) + " => service is corrupt");
            } else {
                updateFlags(flag, false);
                Log::err(TAG, m_sConnectionName
                    + " => wrong checker response... (not found [service is worked] or [service is corrupt] and you have only 10 sec),\n"
                    "p_stdout =\n" + p_stdout  + "; p_stdout = " + p_stdout);
            }
        }
        SleepSimulator().sleep(m_service.timeSleepBetweenRunScriptsInSec()*1000);
    }
}

void ServiceCheckerThread::updateFlags(QString flag, bool bWorked) {
    QSqlDatabase db = *m_pDatabase;

    QDateTime dateTime = QDateTime::currentDateTime();
    QString dateTimeString1 = dateTime.toString("yyyy-MM-dd hh:mm:ss");
        
    dateTime = dateTime.addSecs(m_config.flagTimeliveInMin()*60); // add 10 minutes. it is live of flag
    QString dateTimeString2 = dateTime.toString("yyyy-MM-dd hh:mm:ss");
    
    {
        QSqlQuery query(*m_pDatabase);
        QString strQuery = "INSERT INTO flags_live(serviceid, flag, teamid, date_start, date_end, user_passed) VALUES("
            + QString::number(m_service.num()) + ", "
            + "'" + flag + "', "
            + QString::number(m_team.num()) + ", "
            + "'" + dateTimeString1 + "', "
            + "'" + dateTimeString2 + "', "
            + "0"
        + ");";
        query.exec(strQuery);
    }

    {
        QSqlQuery query(*m_pDatabase);
        query.prepare("UPDATE scoreboard SET "
        " score = :score, "
        " service_status = :service_status,"
        " date_change = NOW() "
        " WHERE name = :name AND teamid = :teamid");
        query.bindValue(":score", 0);
        query.bindValue(":service_status", bWorked ? "ok" : "corrupt");
        query.bindValue(":name", m_service.id());
        query.bindValue(":teamid", m_team.num());
        if(!query.exec()){
            Log::err(TAG, "Query failed \n\n" + db.lastError().text());
        }
    }

    // moving from flags_live to flags
    {
        // TODO check every flag on team service in another thread
        QString strQuery = "SELECT id FROM flags_live where "
            " teamid = " + QString::number(m_team.num()) + " "
            " and serviceid = " + QString::number(m_service.num()) + " "
            " and (date_end < NOW() or user_passed > 0)";
        QSqlQuery query(db);
        if(!query.exec(strQuery))
            Log::err(TAG, "Query failed: " + strQuery + " \n\n" + db.lastError().text());
        
        while(query.next()) {
            QSqlRecord record = query.record();
            int id = record.value("id").toInt();
            QString strQueryInsert = "INSERT flags(serviceid, flag, teamid, date_start, date_end, user_passed) "
                " SELECT serviceid, flag, teamid, date_start, date_end, user_passed "
                " FROM flags_live WHERE id = " + QString::number(id);
            
            QSqlQuery query_insert(db);
            if(!query_insert.exec(strQueryInsert))
                Log::err(TAG, "Query failed: " + strQueryInsert + " \n\n" + db.lastError().text());
            
            QSqlQuery query_delete(db);
            QString strQueryDelete = "DELETE FROM flags_live WHERE id = " + QString::number(id);
            if(!query_delete.exec(strQueryDelete)) 
                Log::err(TAG, "Query failed: " + strQueryDelete + " \n\n" + db.lastError().text());
        }
    }
    
    // update scoreboard lost
    {
        QString strId = QString::number(m_team.num());
        QString strQuery = " UPDATE "
            "	scoreboard "
            "SET "
            "	date_change = NOW(), "
            "	score = (SELECT SUM(t.cnt) AS cnt FROM ( "
            "		SELECT COUNT(*) as cnt FROM flags WHERE user_passed <> 0 AND teamid = " + strId + 
            "		UNION ALL SELECT COUNT(*) as cnt FROM flags_live WHERE user_passed <> 0 AND teamid = " + strId + ") t)"
            " WHERE teamid = " + strId + " AND name = 'Lost';";

        QSqlQuery query(db);
        if(!query.exec(strQuery)){
            Log::err(TAG, "Query failed: " + strQuery + " \n\n" + query.lastError().text());
        }
    }

    if (bWorked) {
        // update scoreboard defence
        {
            QString strId = QString::number(m_team.num());
            QString strQuery = "UPDATE scoreboard "
                "   SET "
                "       date_change = NOW(), "
                "       score = (SELECT COUNT(*) from flags where teamid = " + strId + " and user_passed = 0)"
                "   WHERE teamid = " + strId + " and name = 'Defence';";
            QSqlQuery query(db);
            if(!query.exec(strQuery))
                Log::err(TAG, "Query failed: " + strQuery + " \n\n" + db.lastError().text());
        }

        // update scoreboard attack
        /*{
            QString strId = QString::number(m_team.num());
            QString strQuery = " UPDATE "
				"	scoreboard "
				" SET "
				"	date_change = NOW(),"
				"	score = (SELECT SUM(t.cnt) AS cnt FROM ("
				"		SELECT COUNT(*) as cnt FROM flags WHERE user_passed = " + strId +
				"		UNION ALL SELECT COUNT(*) as cnt FROM flags_live WHERE user_passed = '.$teamid.') t)"
				"WHERE teamid = " + strId + " AND name = 'Attack'";

            QSqlQuery query(db);
            if(!query.exec(strQuery)){
                Log::err(TAG, "Query failed: " + strQuery + " \n\n" + query.lastError().text());
            }
        }*/

        // update scoreboard attack
        {
            QString strId = QString::number(m_team.num());
            QString strQuery = " UPDATE "
				"	scoreboard "
				" SET "
				"	date_change = NOW(),"
				"	score = (SELECT SUM(t.cnt) AS cnt FROM ("
				"		SELECT COUNT(*) as cnt FROM flags WHERE user_passed = " + strId +
				"		UNION ALL SELECT COUNT(*) as cnt FROM flags_live WHERE user_passed = " + strId + ") t)"
				"WHERE teamid = " + strId + " AND name = 'Attack'";

            QSqlQuery query(db);
            if(!query.exec(strQuery)){
                Log::err(TAG, "Query failed: " + strQuery + " \n\n" + query.lastError().text());
            }
        }

        // update summary
        {
            QString strUserId = QString::number(m_team.num());

            QString strQuerySelect = "select ifnull(sum(score),0) as sm from scoreboard where (name = 'Defence' or name = 'Attack') and teamid = " + strUserId + " ";
            QSqlQuery query_select(db);
            if(!query_select.exec(strQuerySelect))
                Log::err(TAG, "Query failed: " + strQuerySelect + " \n\n" + db.lastError().text());
            
            int score = 0;
            if (query_select.next()) {
                QSqlRecord record = query_select.record();
                score = record.value("sm").toInt();
            }

            QString strQueryUpdate = "update scoreboard set date_change = NOW(), score = " + QString::number(score) + " where teamid = " + strUserId + " and name = 'Summary' ";
            QSqlQuery query_update(db);
            if(!query_update.exec(strQueryUpdate))
                Log::err(TAG, "Query failed: " + strQueryUpdate + " \n\n" + db.lastError().text());
        }
    }
    // db.close();
    return;
}
// ---------------------------------------------------------------------    

SleepSimulator::SleepSimulator() {
    localMutex.lock();
}

void SleepSimulator::sleep(unsigned long sleepMS) {
    sleepSimulator.wait(&localMutex, sleepMS);
}

void SleepSimulator::CancelSleep() {
    sleepSimulator.wakeAll();
}