#ifndef HTTP_HANDLER_H
#define HTTP_HANDLER_H

#include <utils_light_http_server.h>

#include <QString>

class HttpHandler : IHttpHandler{
    public:
        HttpHandler(const std::string &sScoreboardHtmlFolder);
        virtual bool handle(
            const std::string &sType,
            const std::string &sPath,
            const std::string &sRequest,
            std::string &sResponse);

    private:
        std::string m_sIndexHtml;
};

#endif // HTTP_HANDLER_H
