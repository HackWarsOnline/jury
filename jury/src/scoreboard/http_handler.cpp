#include "http_handler.h"
#include <fstream>
#include <string>

HttpHandler::HttpHandler(const std::string &sScoreboardHtmlFolder){
    // TODO load body from file
    std::string fullPath = sScoreboardHtmlFolder + "/index.html";
    std::ifstream ifs(fullPath.c_str());

    m_sIndexHtml.assign( (std::istreambuf_iterator<char>(ifs) ),
                (std::istreambuf_iterator<char>()));
}

bool HttpHandler::handle(
    const std::string &sType,
    const std::string &sPath,
    const std::string &sRequest,
    std::string &sResponse){
    if(sPath == "/"){
		sResponse = "HTTP/1.1 200 OK\n"
			"Date: Tue, 14 Aug 2018 08:10:42 GMT\n" // TODO
			"Server: RelertJury\n"
			"Last-Modified: Wed, 22 Jul 2009 19:15:56 GMT\n" // TODO
			"Content-Length: " + std::to_string(m_sIndexHtml.length()) + "\n"
			"Content-Type: text/html\n"
			"Connection: Closed\n"
			"\n" + m_sIndexHtml;
		return true;
	}
	return false;
}