#ifndef CONFIG_H
#define CONFIG_H

#include <istorage.h>
#include <model_team.h>
#include <model_service.h>

#include <QString>
#include <QVector>

class JuryConfiguration {
	public:
		JuryConfiguration();
		bool readFromFile(QString sConfigFile);
		bool exportToConfigPHP();
		bool initScoreboardTable();
		int flagTimeliveInMin();
		QVector<ModelTeam> &teams();
		QVector<ModelService> &services();
		int scoreboardPort();
		std::string scoreboardHtmlFolder();
		QString databaseHost();
		int databasePort();
		QString databaseName();
		QString databaseUser();
		QString databasePass();
		QString gameName();
		
	private:
		QString TAG;
		IStorage *m_pStorage;
		int m_nScoreboardPort;
		std::string m_sScoreboardHtmlFolder;

		QString m_sDatabaseHost;
		int m_nDatabasePort;
		QString m_sDatabaseName;
		QString m_sDatabaseUser;
		QString m_sDatabasePass;
		int m_nFlagTimeliveInMin;
		QString m_sGameName;
		QVector<ModelTeam> m_vTeams;
		QVector<ModelService> m_vServices;

};

#endif // CONFIG
