#include "utils_logger.h"
#include <unistd.h>
#include <QtCore>
#include <QString>
#include <QFile>
#include <QVector>
#include <QThread>
#include <QTextStream>
#include <QMap>
#include <QList>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QProcess>
#include <QUuid>
#include <QRegExp>
#include <iostream>
#include <iostream>
#include <thread>
#include <time.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <math.h>
#include <sstream>


// Last log messages
std::mutex *g_LOG_MUTEX = NULL;

// ---------------------------------------------------------------------

void Log::info(QString tag, QString msg){
	Color::Modifier def(Color::FG_DEFAULT);
    Log::add(def, "INFO",tag.toStdString(), msg.toStdString());
}

// ---------------------------------------------------------------------

void Log::err(QString tag, QString msg){
	Color::Modifier red(Color::FG_RED);
    Log::add(red, "ERR",tag.toStdString(), msg.toStdString());
}

// ---------------------------------------------------------------------

void Log::ok(QString tag, QString msg){
	Color::Modifier green(Color::FG_GREEN);
    Log::add(green, "OK",tag.toStdString(), msg.toStdString());
}

// ---------------------------------------------------------------------

void Log::warn(QString tag, QString msg){
	Color::Modifier yellow(Color::FG_YELLOW);
    Log::add(yellow, "WARN",tag.toStdString(), msg.toStdString());
}

// ---------------------------------------------------------------------

void Log::initGlobalVariables(){
    // create mutex if not created
    if(g_LOG_MUTEX == NULL){
        g_LOG_MUTEX = new std::mutex();
        // std::cout << Log::currentTime() + ", " + Log::threadId() + " Init mutex for log\r\n";
    }
}

// ---------------------------------------------------------------------

std::string Log::currentTime(){
    // milleseconds
    struct timeval tv;
    gettimeofday(&tv, NULL);
    int millisec = lrint(tv.tv_usec/1000.0); // Round to nearest millisec
    if (millisec>=1000) { // Allow for rounding up to nearest second
        millisec -=1000;
        tv.tv_sec++;
    }

    // datetime
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);

    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%Y-%m-%d %X.", &tstruct);
    return std::string(buf) + std::to_string(millisec);
}

// ---------------------------------------------------------------------

std::string Log::threadId(){
    std::thread::id this_id = std::this_thread::get_id();
    std::stringstream stream;
    stream << std::hex << this_id;
    return "0x" + std::string(stream.str());
}

// ---------------------------------------------------------------------

void Log::add(Color::Modifier clr, const std::string &sType, const std::string &sTag, const std::string &sMessage){
    Log::initGlobalVariables();

    g_LOG_MUTEX->lock();
    Color::Modifier def(Color::FG_DEFAULT);
    std::string sLogMessage = Log::currentTime() + ", " + Log::threadId() + " [" + sType + "] " + sTag + ": " + sMessage;
    std::cout << clr << sLogMessage << def << "\r\n";

 	// write to file
	QFile file("/usr/share/relertjury/jury.log");
	if (file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)) {
		QDateTime dateTime = QDateTime::currentDateTime();
		QString dateTimeString = dateTime.toString("[yyyy-MMM-dd hh:mm:ss] ");
		QTextStream stream( &file );
		stream << dateTimeString << QString::fromStdString(sLogMessage) << endl;
		file.close();
	}
    g_LOG_MUTEX->unlock();
}