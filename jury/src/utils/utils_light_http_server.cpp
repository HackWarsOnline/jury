#include "utils_light_http_server.h"
#include "utils_logger.h"
#include <sstream>
#include <iostream>
#include <vector>

bool HandlerRequestHelloWorld::handle(const std::string &sType,
		const std::string &sPath,
		const std::string &sRequest,
		std::string &sResponse){
	if(sPath == "/"){
		std::string responseBody = ""
			"<html><body>"
			"<h1>Hello, World! So...</h1>"
			"</body>"
			"</html>";

		sResponse = "HTTP/1.1 200 OK\n"
			"Date: Tue, 14 Aug 2018 08:10:42 GMT\n"
			"Server: RelertJury\n"
			"Last-Modified: Wed, 22 Jul 2009 19:15:56 GMT\n"
			"Content-Length: " + std::to_string(responseBody.length()) + "\n"
			"Content-Type: text/html\n"
			"Connection: Closed\n"
			"\n" + responseBody;
		return true;
	}
	return false;
}


// ----------------------------------------------------------------------
// LightHttpRequest

LightHttpRequest::LightHttpRequest(int nSockFd, const std::string &sAddress, IHttpHandler *pHandler) {
	m_nSockFd = nSockFd;
	m_sAddress = sAddress;
	m_pHandler = pHandler;
}

// ----------------------------------------------------------------------

void LightHttpRequest::sendResponse(const std::string &msg) {
	send(m_nSockFd, msg.c_str(), msg.length(),0);
}

int LightHttpRequest::sockFd(){
	return m_nSockFd;
}

// ----------------------------------------------------------------------

std::string LightHttpRequest::requestType(){
	return m_sRequestType;
}

// ----------------------------------------------------------------------

std::string LightHttpRequest::requestPath(){
	return m_sRequestPath;
}

// ----------------------------------------------------------------------

std::string LightHttpRequest::requestHttpVersion(){
	return m_sRequestHttpVersion;
}

// ----------------------------------------------------------------------

std::string LightHttpRequest::address(){
	return m_sAddress;
}

// ----------------------------------------------------------------------

void LightHttpRequest::parseRequest(const std::string &sRequest){
    std::istringstream f(sRequest);
	std::string firstLine = "";
    if(getline(f, firstLine, '\n')) {
        // nothing
    }

	if(firstLine.size() > 0){
		std::istringstream f(firstLine);
		std::vector<string> params;
		std::string s;
		while (getline(f, s, ' ')) {
			std::cout << s << endl;
			params.push_back(s);
		}
		if(params.size() > 0){
			m_sRequestType = params[0];
		}

		if(params.size() > 1){
			m_sRequestPath = params[1];
		}

		if(params.size() > 2){
			m_sRequestHttpVersion = params[2];
		}
	}
}

// ----------------------------------------------------------------------

std::string LightHttpRequest::handle(const std::string &sRequest){
	this->parseRequest(sRequest);
	if(this->requestType() != "GET"){
		return this->response("HTTP/1.1 501 Not Implemented");
	}

	if(m_pHandler != NULL){
		std::string sResponse = "";
		if(m_pHandler->handle(
			this->requestType(),
			this->requestPath(),
			sRequest, sResponse
		)){
			return sResponse;
		}else{
			return this->response("HTTP/1.1 404 Not Found");
		}
	}

	return this->response("HTTP/1.1 500 Internal Server Error");

	/*std::string responseBody = "";
	responseBody += "<html><body>";
	responseBody += "<h1>Hello, World!</h1>";
	responseBody += "</body>";
	responseBody += "</html>";

	std::string response = "HTTP/1.1 200 OK\n";
	response += "Date: Tue, 14 Aug 2018 08:10:42 GMT\n";
	response += "Server: RelertJury\n";
	response += "Last-Modified: Wed, 22 Jul 2009 19:15:56 GMT\n";
	response += "Content-Length: " + std::to_string(responseBody.length()) + "\n";
	response += "Content-Type: text/html\n";
	response += "Connection: Closed\n";
	response += "\n";
	response += responseBody;
	return response;*/
}

// ----------------------------------------------------------------------

std::string LightHttpRequest::response(const std::string &sFirst, const std::string &sBody){
	return sFirst + "\n"
		"Date: Tue, 14 Aug 2018 08:10:42 GMT\n" // TODO generate data
		"Server: RelertJury\n" // TODO generate data
		"Last-Modified: Wed, 22 Jul 2009 19:15:56 GMT\n" // TODO generate data
		"Content-Length: " + std::to_string(sBody.length()) + "\n" // TODO generate data
		"Content-Type: text/html\n" // TODO generate data
		"Connection: Closed\n" // TODO generate data
		"\n" + sBody;
}

// ----------------------------------------------------------------------
// newRequest

void* newRequest(void *arg) {
	Log::info("newRequest", "");
	LightHttpRequest *info = (LightHttpRequest *)arg;
	int nSockFd = info->sockFd();
	int n;
	// int newsockfd = (long)arg;
	char msg[MAXPACKETSIZE];
	memset(msg, 0, MAXPACKETSIZE);

	pthread_detach(pthread_self());
	std::string sRequest;
	
	std::cout << nSockFd  << ": address = " << info->address() << "\n";

	// read data from socket
	while(1) { // problem can be here
		std::cout << nSockFd  << ": wait recv...\n";
		n = recv(nSockFd, msg, MAXPACKETSIZE, 0);
		std::cout << "N: " << n << std::endl;
		if (n == -1) {
			std::cout << nSockFd  << ": error read... \n";
			break;
		}
		if (n == 0) {
		   //close(nSockFd);
		   break;
		}
		msg[n]=0;
		//send(newsockfd,msg,n,0);
		sRequest = std::string(msg);
		// stop reading
		int len = sRequest.length();
		// TODO 
		if(len > 4 && ((sRequest[len-1] == '\n' && sRequest[len-2] == '\r' && sRequest[len-3] == '\n' && sRequest[len-4] == '\r')
				|| (sRequest[len-1] == '\n' && sRequest[len-2] == '\n'))
		){
			std::cout << nSockFd  << ": end of request\n";
			break;
		}
		// usleep(100);
    }
	std::cout << nSockFd  << ": request >>>> \n" << sRequest << "\n <<<<< request\n";

	std::string sResponse = info->handle(sRequest);
	std::cout << nSockFd << ": response >>>> \n" << sResponse << "\n <<<< response\n";
	send(nSockFd, sResponse.c_str(), sResponse.length(),0);

	// TODO handle and send
	std::cout << nSockFd  << ": close\n";
	close(nSockFd);
	return 0;
}

// ----------------------------------------------------------------------
// LightHttpServer

LightHttpServer::LightHttpServer() {
	TAG = "LightHttpServer";
}

// ----------------------------------------------------------------------

void LightHttpServer::start(int nPort, IHttpHandler *pHandler) {
	m_pHandler = pHandler;
	m_nSockFd = socket(AF_INET, SOCK_STREAM, 0);
	if(m_nSockFd <= 0){
		Log::err(TAG, "Failed to establish socket connection");
		return;
	}
 	memset(&m_serverAddress, 0, sizeof(m_serverAddress));
	m_serverAddress.sin_family = AF_INET;
	m_serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	m_serverAddress.sin_port = htons(nPort);
	if(bind(m_nSockFd, (struct sockaddr *)&m_serverAddress, sizeof(m_serverAddress)) == -1){
		Log::err(TAG, "Error binding to port " + QString::number(nPort));
		return;
	}
 	listen(m_nSockFd, 5);
	Log::info("LightHttpServer", "Light Http Server started on " + QString::number(nPort) + " port.");
	std::string str;
	while(1) { // or problem can be here
		struct sockaddr_in clientAddress;
		socklen_t sosize  = sizeof(clientAddress);
		int nSockFd = accept(m_nSockFd,(struct sockaddr*)&clientAddress,&sosize);
		std::string sAddress = inet_ntoa(clientAddress.sin_addr);
		LightHttpRequest *info = new LightHttpRequest(nSockFd, sAddress, m_pHandler);
		pthread_create(&m_serverThread, NULL, &newRequest, (void *)info);
		std::cout << "wait \n";
		usleep(1000);
	}
}

void LightHttpServer::stop()
{
	close(m_nSockFd);
	// close(newsockfd);
} 
