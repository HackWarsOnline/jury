#ifndef UTILS_LOG_H
#define UTILS_LOG_H
 
#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QSqlDatabase>
#include "config.h"
#include <QString>
#include <stdio.h>
#include <iostream>
#include <mutex>
#include <deque>
#include "colormod.h"

class Log {
    public:
        static void info(QString tag, QString msg);
        static void err(QString tag, QString msg);
        static void warn(QString tag, QString msg);
        static void ok(QString tag, QString msg);
        static void initGlobalVariables();
        static std::string currentTime();
        static std::string threadId();

	private:
        static void add(Color::Modifier clr, const std::string &sType, const std::string &sTag, const std::string &sMessage);
};

extern std::mutex *g_LOG_MUTEX;

#endif // UTILS_LOG_H
