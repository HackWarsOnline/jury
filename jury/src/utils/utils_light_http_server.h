#ifndef TCP_SERVER_H
#define TCP_SERVER_H

#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <QString>

using namespace std;

#define MAXPACKETSIZE 4096

class IHttpHandler {
	public:
		virtual bool handle(
			const std::string &sType,
			const std::string &sPath,
			const std::string &sRequest,
			std::string &sResponse) = 0;
};

class HandlerRequestHelloWorld : IHttpHandler {
	public:
		virtual bool handle(
			const std::string &sType,
			const std::string &sPath,
			const std::string &sRequest,
			std::string &sResponse);
};

class LightHttpRequest {
	public:
		LightHttpRequest(int nSockFd, const std::string &sAddress, IHttpHandler *pHandler);
		void sendResponse(const std::string &sMessage);
		int sockFd();
		void parseRequest(const std::string &sRequest);
		std::string address();
		std::string requestType();
		std::string requestPath();
		std::string requestHttpVersion();
		std::string handle(const std::string &sRequest);
		std::string response(const std::string &sFirst, const std::string &sBody = "");

	private:
		int m_nSockFd;
		IHttpHandler *m_pHandler;
		std::string m_sAddress;
		std::string m_sRequestType;
		std::string m_sRequestPath;
		std::string m_sRequestHttpVersion;
};

static void * newRequest(void * argv);

class LightHttpServer {
	public:
		LightHttpServer();
		void start(int nPort, IHttpHandler *pHandler);
		void stop();
		int n, pid;
		
		

	private:
		QString TAG;
		pthread_t m_serverThread;
		int m_nSockFd;
		IHttpHandler *m_pHandler;
		struct sockaddr_in m_serverAddress;	
};

#endif
