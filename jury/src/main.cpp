#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <syslog.h>
#include <QtCore>
#include <QString>
#include "service_checker_thread.h"
#include "model_team.h"
#include <utils_logger.h>
#include <utils_light_http_server.h>
#include <http_handler.h>

LightHttpServer http;

void quitApp(int signum) {
  cout << endl << "Terminating server" << endl;
  http.stop();
  exit(1);
}

int main(int argc, char* argv[]) {
	QString TAG = "MAIN";

	std::cout << "Logfile: /usr/share/relertjury/jury.log\n\n";
	QString sett_file = "/usr/share/relertjury/conf.d/conf.ini";

	JuryConfiguration config;

	if(!config.readFromFile(sett_file)){
		Log::err(TAG, "Configuration file has some problems");
		return -1;
	}

	// parse attributes
	std::vector<std::string> vArgs;
	for(int i = 0; i < argc; i++){
		vArgs.push_back(argv[i]);
		std::cout << i << " => " << argv[i] << "\n";
	}

	if(vArgs.size() > 1 && vArgs[1] == "http_test"){
		Log::ok(TAG, "Start http_test on " + QString::number(config.scoreboardPort()));
		HttpHandler *pScoreboard = new HttpHandler(config.scoreboardHtmlFolder());
		signal( SIGINT, quitApp );
  		signal( SIGTERM, quitApp );
		http.start(config.scoreboardPort(), (IHttpHandler *)pScoreboard); // will be block thread
		return 0;
	}
	
	// prepare to start server
	if(!config.exportToConfigPHP()){
		Log::err(TAG, "Could not export to config.php");
		return -1;
	}

	if(!config.initScoreboardTable()){
		Log::err(TAG, "Could not init scoreboard table");
		return -1;
	}

	// stop
	// return -1;

	// TODO check scoreboard table
	// TODO prepare config.php

	Log::info(TAG, "Starting...");
    
	QVector<ServiceCheckerThread *> threads;
	
	for (int iservice = 0; iservice < config.services().size(); iservice++) {
		for (int iteam = 0; iteam < config.teams().size(); iteam++) {
			ModelTeam team = config.teams()[iteam];
			ModelService service = config.services()[iservice];

			QString strLog = "Start thread for " + team.id() + "_" + service.id() + "\n";
			Log::info(TAG, strLog);
			ServiceCheckerThread *thr = new ServiceCheckerThread(config, team, service);
			thr->start();
			threads.push_back(thr);
		}
	}

	while(1) {
		Log::info(TAG, "wait 2 minutes");
		SleepSimulator().sleep(120000);
		Log::info(TAG, "wait ended");
	}
	return 0;
}
