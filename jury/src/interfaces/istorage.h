#ifndef ISTORAGE_H
#define ISTORAGE_H

#include <string>

class IStorage {
    public:
        virtual std::string type() = 0;
        virtual bool readFromFile(const std::string &sFilePath) = 0;
        
};

#endif // ISTORAGE_H