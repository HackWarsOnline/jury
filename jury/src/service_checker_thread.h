#ifndef SERVICE_CHECKER_THREAD_H
#define SERVICE_CHECKER_THREAD_H
 
#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QSqlDatabase>
#include "config.h"
#include "model_team.h"
#include "model_service.h"

// struct 

class ServiceCheckerThread : public QThread
{
	public:
		ServiceCheckerThread(const JuryConfiguration &config, const ModelTeam &team, const ModelService &service);
		void run();

	private:
		QString TAG;
		ModelTeam m_team;
		ModelService m_service;
		QString m_sConnectionName;
		QSqlDatabase *m_pDatabase;
		JuryConfiguration m_config;
		void updateFlags(QString flag, bool bWorked);
};

class SleepSimulator {
     QMutex localMutex;
     QWaitCondition sleepSimulator;

public:
    SleepSimulator();
    void sleep(unsigned long sleepMS);
    void CancelSleep();
};

#endif // SERVICE_CHECKER_THREAD_H
