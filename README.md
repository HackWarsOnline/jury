# hackwars-jury-ad

## Download and basic configuration

```
$ sudo apt install git-core
$ cd ~ 
$ git clone http://gitlab.com/relert/jury.git jury.git
$ cd jury.git
$ sudo ln -s `pwd`/. /usr/share/relertjury
$ nano ~/jury.git/conf.d/conf.ini
```

**Look and edit /usr/share/relertjury/conf.d/conf.ini** - more information inside conf.inf in comments 

## Build jury system

```
$ sudo apt install make qtchooser qt5-default libqt5sql5-mysql # requriments
$ cd ~/jury.git/jury
$ ./clean.sh 
$ ./build.sh
```

Before you can check and clean previously data

```
$ cd ~/jury.git/jury
$ ./jury clean # not implemented yet
```

## Run Jury System

```
$ ./jury
```

## Scoreboard && Acceptance of flag

### Install requriments

```
$ sudo apt install apache2 # web server
$ sudo apt install php # php + pdo + mysql
```

### Install sources to www folder

Move 
```
$ sudo mkdir /var/www/html/relertjury
$ sudo cp html/* /var/www/html/relertjury
```
OR just link
```
$ sudo ln -s `pwd`/html /var/www/html/relertjury
```

* Scoreboard: http://localhost/relertjury/index.php
* Acceptance of flag: http://localhost/relertjury/flag.php

### How to send flag

Get request to http://{HOST}/relertjury/flag.php?teamid={TEAMID}&flag={FLAG}

Where 

* {HOST} - host or ip, where jury system started
* {TEAMID} - number, your unique teamid (see scoreboard)
* {FLAG} - uuid, so... it's flag from enimy server


Example (curl):
```
$ curl http://192.168.1.10/relertjury/flag.php?teamid=1&flag=123e4567-e89b-12d3-a456-426655440000
```

### Apache config sample

```
<VirtualHost *:80>
    Options -Indexes FollowSymLinks MultiViews
    DocumentRoot /var/www/html/
    # ServerName jury.relert.online
    ErrorLog /var/log/apache2/error_log
    CustomLog /var/log/apache2/access_log common


    Alias "/relertjury" "/usr/share/relertjury/html/"
    <Directory "/usr/share/relertjury/html">
        AllowOverride None
        Options -Indexes
        Order allow,deny
        Allow from all
    </Directory>
</VirtualHost>
```


## MySQL Database  (use_storage is mysql)

### Install requriments

```
$ sudo apt install mysql-server
$ sudo apt install mysql-client
```

### Create database

```
> CREATE DATABASE relertjury CHARACTER SET utf8 COLLATE utf8_general_ci;
> CREATE USER 'relertjury'@'localhost' IDENTIFIED BY 'relertjury';
> GRANT ALL PRIVILEGES ON relertjury.* TO 'relertjury'@'localhost' WITH GRANT OPTION;
> FLUSH PRIVILEGES;
```

```
mysql -p -u relertjury relertjury < sql/mysql_relertjury.sql
```






