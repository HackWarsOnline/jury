#!/bin/bash

# must running in root of repository folder

# backup
BACKUP_DIR="`pwd`/../autobackups"
echo "BACKUP_DIR = $BACKUP_DIR"

if [ ! -d $BACKUP_DIR ]; then
  mkdir $BACKUP_DIR
fi

DBNAME=relertjury
DBUSER=relertjury
DBPASS=relertjury

# dump mysql  
if [ -d $1 ]; then
  MYSQLDUMP_SQL="$BACKUP_DIR/relertjury_sql_`date +%Y%m%d-%H%M%S`.sql"
  MYSQLDUMP_TAR_GZ="$BACKUP_DIR/relertjury_sql_`date +%Y%m%d-%H%M%S`.tar.gz"
  echo "MYSQLDUMP_SQL = $MYSQLDUMP_SQL"
  echo "MYSQLDUMP_TAR_GZ = $MYSQLDUMP_TAR_GZ"
  mysqldump -u$DBUSER \
  -p$DBPASS \
  $DBNAME \
  > $MYSQLDUMP_SQL
  tar -zcvf $MYSQLDUMP_TAR_GZ $MYSQLDUMP_SQL
  rm $MYSQLDUMP_SQL
fi
