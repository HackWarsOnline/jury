<?php
	if (!isset($_GET['flag']) || !isset($_GET['teamid']))
	{
		echo '
			<form>
				<table>
					<tr>
						<td>Flag:</td>
						<td><input type="text" name="flag" value=""/></td>
					</tr>
					<tr>
						<td>ID Team:</td>
						<td><input type="text" name="teamid" value=""/></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" value="Send"/></td>
					</tr>
				</table>
			</form>
		';
		exit;
	}
	$flag_was_not_accepted = "[FLAG WAS NOT ACCEPTED]";
	
	if(!isset($_GET['teamid']) && !isset($_GET['flag'])) {
		$db->close();
		die($flag_was_not_accepted);
	}
	
	$teamid = $_GET['teamid'];
	$flag = strtoupper($_GET['flag']);
	
	if (!is_numeric($teamid)) {
		$db->close();
		die($flag_was_not_accepted." (4 - Team id must be number)");
	}
	$teamid = (int)$teamid;
	
	// check team
	include("config.php");

	while(!isset($teams['team'.$teamid])) {
		die($flag_was_not_accepted." (5 - 'teamid' not found)");
	}

	$conn = new PDO('mysql:host=localhost;dbname='.$database['name'],
		$database['user'],
		$database['pass']);

	// 6a331fd2-133a-4713-9587-12652d34666d
	if (!preg_match("/^[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}$/", $flag))
		die($flag_was_not_accepted." (6 - flag has wrong format)");

	$stmt = $conn->prepare('SELECT *, date_end - NOW() AS ti FROM flags_live WHERE flag = ?');
	$stmt->execute(array($flag));
	$row = $stmt->fetch();

	if (!$row) {
		die($flag_was_not_accepted." (7 - flag is not exists or very old)");
	} else {
		if ($row['ti'] < 0) {
			die($flag_was_not_accepted." (8 - flag is old)");
		}
		if ($row['teamid'] == $teamid) {
			die($flag_was_not_accepted." (9 - it is your flag)");
		}
		if ($row['user_passed'] != $teamid && $row['user_passed'] != 0) {
			die($flag_was_not_accepted." (10 - flag is already passed)");
		}
		if ($row['user_passed'] == $teamid) {
			die($flag_was_not_accepted." (11 - flag you're already passed)");
		}
		
		$serviceid = $row['serviceid'];
		$stmt = $conn->prepare('SELECT service_status FROM scoreboard sc WHERE sc.teamid = ? AND sc.name = ?');
		$stmt->execute(array($teamid, 'service'.$serviceid));

		if($row = $stmt->fetch()){
			if ($row['service_status'] != 'ok') {
				die($flag_was_not_accepted." (12 - your service is currupt)");
			}
			// ok
		}else{
			die($flag_was_not_accepted." (14 - your service not found)");
		}
	}

	// update flag
	$stmt = $conn->prepare('UPDATE flags_live SET user_passed = ? WHERE flag = ? AND date_end > NOW() AND teamid <> ? AND user_passed = 0');
	$stmt->execute(array($teamid, $flag, $teamid));

	$stmt = $conn->prepare('SELECT *, date_end - NOW() AS ti FROM flags_live WHERE flag = ?');
	$stmt->execute(array($flag));
	if($row = $stmt->fetch()){
		if ($row['user_passed'] == $teamid) {
			$owner = $row['teamid'];

			// update attack score
			$conn->query('UPDATE 
					scoreboard 
				SET 
					date_change = NOW(),
					score = (SELECT SUM(t.cnt) AS cnt FROM (
						SELECT COUNT(*) as cnt FROM flags WHERE user_passed = '.$teamid.'
						UNION ALL SELECT COUNT(*) as cnt FROM flags_live WHERE user_passed = '.$teamid.') t)
				WHERE teamid = '.$teamid.' AND name = "Attack";'); // don't worked prepare() - execute()


			// update lost
			$conn->query('UPDATE 
					scoreboard 
				SET 
					date_change = NOW(),
					score = (SELECT SUM(t.cnt) AS cnt FROM (
						SELECT COUNT(*) as cnt FROM flags WHERE user_passed <> 0 AND teamid = '.$owner.'
						UNION ALL SELECT COUNT(*) as cnt FROM flags_live WHERE user_passed <> 0 AND teamid = '.$owner.') t)
				WHERE teamid = '.$owner.' AND name = "Lost";'); // don't worked prepare() - execute()

			die("FLAG ACCEPTED");
		}else{
			// TODO
		}
	}
	die($flag_was_not_accepted." (13 - Please say about this to admins)");
?>
