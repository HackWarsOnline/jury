# Service Sample

## Start

```
$ python marsetalk.py
```


## Build docker help command

```
$ sudo docker build --rm=true -t "relertjury:service.sample" ./
```

## Docker run

```
$ sudo docker run -t --name=service1 "relertjury:service.sample"
```